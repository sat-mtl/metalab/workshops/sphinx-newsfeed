Elvensense 96 is released
=========================

.. feed-entry::
   :date: 2012-12-31

We are proud to announce a new release of **Elvensense**.

.. cut::

Specific changes since the last release:

* An exciting feature was added.
* An annoying bug was fixed.

