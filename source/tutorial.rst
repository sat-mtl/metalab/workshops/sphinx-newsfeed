Single projector example
========================

.. feed-entry::
   :date: 2021-09-19

This example shows how to project onto a known object for which we have
a 3D model. 

.. cut::

The workflow is as follows:

-  create a 3D model of the object which will serve as a projection
   surface,
-  export from Blender a draft configuration file,
-  open the file in Splash, calibrate the projector,
-  load some contents to play onto the surface.

To do this this tutorial, you will need:

-  Splash installed on a computer
-  a video-projector connected to the computer
-  a box-shaped object

