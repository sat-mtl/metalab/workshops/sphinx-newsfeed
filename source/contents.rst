.. Newsfeed test documentation master file, created by
   sphinx-quickstart on Tue Sep 21 08:09:40 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Newsfeed test's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   
   feed
   tutorial
