# testing sphinxcontrib-newsfeed

https://pypi.org/project/sphinxcontrib-newsfeed/

# requirements

a working sphinx doc setup!

reference: https://gitlab.com/sat-metalab/workshops/sphinx-workshop

only need:

* virtualenv
* install Sphinx
* `sphinx-quickstart`

## create a feed

`pip install sphinxcontrib-newsfeed`

add to `conf.py`: 
    
    extensions.append('sphinxcontrib.newsfeed')

The doc says: 

    Now you can convert any Sphinx document to a news entry by using directive feed-entry.
    
Let's test this in `welcome_feed.rst`:

```
Welcome!!!
==========

.. feed-entry::
   :date: 2021-09-21

Welcome to the news feed of **Elvensense**.  Here we will post
release announcements and other project news.


```

It should be live at https://sat-metalab.gitlab.io/workshops/sphinx-newsfeed/index.rss
